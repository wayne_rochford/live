
 /*
 
 * 
 * Map.java
 * 

 * 
 * @reference https://developers.facebook.com/android/
 * @author Wayne Rochford, x11472332
 * 
 */
package com.dcprod.livem;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

 public class Map extends Activity implements OnMarkerClickListener {
	
	ImageButton im;
	
	GoogleMap mMap;
	private int map01;
	String id;
	private static final String TAG_SEARCH = "search";
	private static final String TAG_ID = "id";
	private static final String TAG_VENUE = "venue";
	 
	 //private static final String TAG_VENUE = "venue";
	 
	 private Marker myMarkerpoint, myMarkeracademy, myMarkerbutfact, myMarkerolympia, myMarkervoodoo, myMarkerwhel, myMarkerworkman, myMarkervicst;  
	 
	EditText artistTxt;

	String search;
	String venue;
	//String venue = "O2";

	final LatLng SPIRE = new LatLng(53.3498223, -6.260351);
	final LatLng O2 = new LatLng(53.3475, -6.2286111);
	final LatLng acad = new LatLng(53.34802343, -6.26197637);
	final LatLng butfact = new LatLng(53.34486504, -6.26457441);
	final LatLng olympia = new LatLng(53.34420094, -6.26605746);
	final LatLng voodoo = new LatLng(53.3466111, -6.27981204);
	final LatLng whel = new LatLng(53.33657654, -6.26564172);
	final LatLng workman = new LatLng(53.3454149, -6.26649219);
	final LatLng vicst = new LatLng(53.34263768, -6.27795692);
	

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);

	    
	    mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
		        .getMap();
	    

		//Move the camera instantly to spire with a zoom of 15.
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(SPIRE, 15));

		// Zoom in, animating the camera.
		mMap.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null); 

	    mMap.setOnMarkerClickListener(this);
	    
 
	  //Assigns variable to locations
	   
		 
	    myMarkerpoint = mMap.addMarker(new MarkerOptions()
        .position(O2)
        .title("O2")
        .snippet("Click to view gigs!")
        .icon(BitmapDescriptorFactory.fromResource(R.drawable.noteicon)));

	    myMarkeracademy = mMap.addMarker(new MarkerOptions()
        .position(acad)
        .title("The Academy")
        .snippet("Click to view gigs!")
        .icon(BitmapDescriptorFactory.fromResource(R.drawable.goldnote)));
	    
	    myMarkerbutfact = mMap.addMarker(new MarkerOptions()
        .position(butfact)
        .title("The Button Factory")
        .snippet("Click to view gigs!")
        .icon(BitmapDescriptorFactory.fromResource(R.drawable.noteicon)));
	    
	    myMarkerolympia = mMap.addMarker(new MarkerOptions()
        .position(olympia)
        .title("The Olympia")
        .snippet("Click to view gigs!")
        .icon(BitmapDescriptorFactory.fromResource(R.drawable.noteicon)));
	    
	    myMarkervoodoo = mMap.addMarker(new MarkerOptions()
        .position(voodoo)
        .title("Voodoo Lounge")
        .snippet("Click to view gigs!")
        .icon(BitmapDescriptorFactory.fromResource(R.drawable.noteicon)));
	    
	    myMarkerwhel = mMap.addMarker(new MarkerOptions()
        .position(whel)
        .title("Whelans")
        .snippet("Click to view gigs!")
        .icon(BitmapDescriptorFactory.fromResource(R.drawable.noteicon)));
	    
	    myMarkerworkman = mMap.addMarker(new MarkerOptions()
        .position(workman)
        .title("THe Workman's Club")
        .snippet("Click to view gigs!")
        .icon(BitmapDescriptorFactory.fromResource(R.drawable.noteicon)));
	
	    myMarkervicst = mMap.addMarker(new MarkerOptions()
        .position(vicst)
        .title("Vicar Street")
        .snippet("Click to view gigs!")
        .icon(BitmapDescriptorFactory.fromResource(R.drawable.noteicon)));

		  im = (ImageButton) findViewById(R.id.searchButton);
			
im.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Intent y = new Intent(Map.this, Main.class);
	               
	                startActivityForResult(y, 0);
	                finish();
				
			}
		});

	}
	
	//This method allows the blimps to be clickable
	
	@Override
	public boolean onMarkerClick(final Marker marker ) {
		// TODO Auto-generated method stub
		

		 if (marker.equals(myMarkerpoint)) 
        {
			
			
			Intent in = new Intent(getApplicationContext(),SearchAllActivity.class);
        	// sending to next activity
			search = "O2";
        	
        	in.putExtra(TAG_SEARCH, search);
        	startActivity(in);
        	
        	
		
        }

		else if (marker.equals(myMarkeracademy)) 
        {
			
			
			Intent in = new Intent(getApplicationContext(),SearchAllActivity.class);
        	// sending to next activity
			search = "The Academy";
        	
        	in.putExtra(TAG_SEARCH, search);
        	startActivity(in);

        }
		
		else if (marker.equals(myMarkerbutfact)) 
        {
			
			
			Intent in = new Intent(getApplicationContext(),SearchAllActivity.class);
        	// sending to next activity
			search = "The Button Factory";
        	
        	in.putExtra(TAG_SEARCH, search);
        	startActivity(in);

		
        }
		
		else if (marker.equals(myMarkerolympia)) 
        {
			
			
			Intent in = new Intent(getApplicationContext(),SearchAllActivity.class);
        	// sending to next activity
			search = "Olympia Theatre";
        	
        	in.putExtra(TAG_SEARCH, search);
        	startActivity(in);

        }
		
		else if (marker.equals(myMarkervoodoo)) 
        {
			
			
			Intent in = new Intent(getApplicationContext(),SearchAllActivity.class);
        	// sending to next activity
			search = "Voodoo Lounge";
        	
        	in.putExtra(TAG_SEARCH, search);
        	startActivity(in);
        	

        }
		
		else if (marker.equals(myMarkerwhel)) 
        {
			
			
			Intent in = new Intent(getApplicationContext(),SearchAllActivity.class);
        	// sending to next activity
			search = "Whelans";
        	
        	in.putExtra(TAG_SEARCH, search);
        	startActivity(in);

		
        }
		
       
		else if (marker.equals(myMarkerworkman)) 
        {
			
			
			Intent in = new Intent(getApplicationContext(),SearchAllActivity.class);
        	// sending to next activity
			search = "The Workman's Club";
        	
        	in.putExtra(TAG_SEARCH, search);
        	startActivity(in);

        }
		
		else if (marker.equals(myMarkervicst)) 
        {
			
			
			Intent in = new Intent(getApplicationContext(),SearchAllActivity.class);
        	// sending to next activity
			search = "Vicar Street";
        	
        	in.putExtra(TAG_SEARCH, search);
        	startActivity(in);
        	
        	
        }

		return false;
		
		
		
		

	}
	

	
	
	
	}
	
	
	
