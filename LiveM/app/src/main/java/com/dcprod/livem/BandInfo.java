package com.dcprod.livem;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("NewApi")
public class BandInfo extends Activity {
	
	ImageButton mcdbut,tm,live,fb,ar,aikenbut,dmebut;
	TextView nameTF, venueTF, timeTF, supportTF, genreTF, priceTF, dateTF;
	ImageView aiken,mcd2,dme;
	

	
	
	String id;
    String artist;
    
    String venue;
	int success;
	
	private ProgressDialog pDialog;
	
	JSONParser jsonParser = new JSONParser();
	
	private static final String url_one = "http://livem.co.nf/get_record.php";
		
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_GIGINFO = "giginfo";
    private static final String TAG_ID = "id";
    private static final String TAG_ARTIST = "artist";
    private static final String TAG_VENUE = "venue";
    private static final String TAG_TIME = "time";
    private static final String TAG_DATE = "date";
    private static final String TAG_GENRE ="genre";
    private static final String TAG_PROMOTER = "promoter";
    private static final String TAG_PRICE = "price";
    private static final String TAG_SUPPORT_ONE = "support_one";
    private static final String TAG_SUPPORT_TWO = "support_two";

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bandinfo);

		StrictMode.enableDefaults();
		
		mcdbut = (ImageButton) findViewById(R.id.mcdbut);
		dmebut = (ImageButton) findViewById(R.id.dmebut);
		aikenbut = (ImageButton) findViewById(R.id.aikenbut);

		tm = (ImageButton) findViewById(R.id.mapviewButton);
		live = (ImageButton) findViewById(R.id.settingsBut);
		fb = (ImageButton) findViewById(R.id.contactBut);
		ar = (ImageButton) findViewById(R.id.imageButton5);

		nameTF = (TextView) findViewById(R.id.nameTF);
		venueTF = (TextView) findViewById(R.id.venueTF);
		timeTF = (TextView) findViewById(R.id.timeTF);
		supportTF = (TextView) findViewById(R.id.supportTF);
		genreTF = (TextView) findViewById(R.id.genreTF);
		priceTF = (TextView) findViewById(R.id.priceTF);
		dateTF = (TextView) findViewById(R.id.dateTF);
		aiken = (ImageView) findViewById(R.id.aiken);
		mcd2 = (ImageView) findViewById(R.id.mcd);
		dme = (ImageView) findViewById(R.id.dme);

		//carry ID over from pressed list option ListViewsImagesActivity.java
		Intent i = getIntent();
		id = i.getStringExtra(TAG_ID);

		new GetgigDetails().execute();

		//dme.setVisibility(View.INVISIBLE);
		//mcd2.setVisibility(View.INVISIBLE);

		tm.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Uri uri = Uri.parse("http://www.ticketmaster.ie/");
  				 Intent intent = new Intent(Intent.ACTION_VIEW, uri);
  				 startActivity(intent);
  				 finish();
				
				
			}
		});

		fb.setOnClickListener(new View.OnClickListener() {
        	
        	
        	
			@Override
			public void onClick(View v) {

               Intent iz = new Intent(BandInfo.this,Facebook.class);
            	// sending to next activity
            	iz.putExtra("messenger", artist);
            	startActivity(iz);
				finish();
			}
			

		});

		ar.setOnClickListener(new View.OnClickListener() {
	
			@Override
			public void onClick(View v) {

				Intent y = new Intent(BandInfo.this, Main.class);
				
				startActivityForResult(y, 0);
				
				finish();

			}
		});
	
	}

    class GetgigDetails extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(BandInfo.this);
            pDialog.setMessage("Loading gig details. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();

        }

        protected String doInBackground(String... params) {
 
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    // Check for success tag
                    try {
                        
                        List<NameValuePair> params = new ArrayList<NameValuePair>();
                        params.add(new BasicNameValuePair("id", id));
                        
                        JSONObject json = jsonParser.makeHttpRequest(url_one, "GET", params);
 
                        Log.d("Single gig Details: ID = " +id, json.toString());
 
                        success = json.getInt(TAG_SUCCESS);
                        if (success == 1 ) {

                        	
                            JSONArray gigObj = json.getJSONArray(TAG_GIGINFO);
 
                             final JSONObject gig = gigObj.getJSONObject(0);
                            
                            venue = gig.getString(TAG_VENUE).toString();
                            artist = gig.getString(TAG_ARTIST).toString();

                    		nameTF = (TextView) findViewById(R.id.nameTF);
                    		venueTF = (TextView) findViewById(R.id.venueTF);
                    		timeTF = (TextView) findViewById(R.id.timeTF);
                    		supportTF = (TextView) findViewById(R.id.supportTF);
                    		genreTF = (TextView) findViewById(R.id.genreTF);
                    		priceTF = (TextView) findViewById(R.id.priceTF);
                    		dateTF = (TextView) findViewById(R.id.dateTF);

                    		nameTF.setText(gig.getString(TAG_PROMOTER)+" presents: "+ gig.getString(TAG_ARTIST));
                            
                    		venueTF.setText("Venue: "+gig.getString(TAG_VENUE));
                            //check if empty first

                            	live.setOnClickListener(new View.OnClickListener() {
                    			
                    			@Override
                    			public void onClick(View v) {
                    				
            		                Intent in = new Intent(getApplicationContext(),Map.class);
            	                	// sending to next activity
            	                	in.putExtra("messenger", venue);
            	                	startActivity(in);
                    			}
                    		});

                    		if(gig.getString(TAG_PROMOTER).equals("MCD")){
                    			
                    			mcd2.setVisibility(View.VISIBLE);
                    			aiken.setVisibility(View.INVISIBLE);
                    			dme.setVisibility(View.INVISIBLE);
                    			mcdbut.setVisibility(View.VISIBLE);
                    			aikenbut.setVisibility(View.INVISIBLE);
                    			dmebut.setVisibility(View.INVISIBLE);

                    			mcdbut.setOnClickListener(new View.OnClickListener() {
                    				
                    				@Override
                    				public void onClick(View v) {
                    				 // TODO Auto-generated method stub
                            		 Uri uri = Uri.parse("http://www.mcd.ie");
                       				 Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                       				 startActivity(intent);
                       				 finish();
                            			
                    				}
                    			});

                    		}
                    		
                    		else if(gig.getString(TAG_PROMOTER).equals("DME")){
                    			
                    			dme.setVisibility(View.VISIBLE);
                    			aiken.setVisibility(View.INVISIBLE);
                    			mcd2.setVisibility(View.INVISIBLE);
                    			dmebut.setVisibility(View.VISIBLE);
                    			mcdbut.setVisibility(View.INVISIBLE);
                    			aikenbut.setVisibility(View.INVISIBLE);
                    			dmebut.setOnClickListener(new View.OnClickListener() {
                    				
                    				@Override
                    				public void onClick(View v) {
                    				// TODO Auto-generated method stub

                            		 Uri uri = Uri.parse("http://www.dme-promotions.com/");
                       				 Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                       				 startActivity(intent);
                       				 finish();
                            			
                    				}
                    			});
                    			
                    		}
                    		
                    		else if(gig.getString(TAG_PROMOTER).equals("Aiken")){
                    			
                    			mcd2.setVisibility(View.INVISIBLE);
                    			dme.setVisibility(View.INVISIBLE);
                    			aiken.setVisibility(View.VISIBLE);
                    			
                    			aikenbut.setVisibility(View.VISIBLE);
                    			mcdbut.setVisibility(View.INVISIBLE);
                    			dmebut.setVisibility(View.INVISIBLE);
                    			
                    			aikenbut.setOnClickListener(new View.OnClickListener() {
                    				
                    				@Override
                    				public void onClick(View v) {
                    					// TODO Auto-generated method stub

                            		Uri uri = Uri.parse("http://www.aikenpromotions.com/");
                       				 Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                       				 startActivity(intent);
                       				 finish();
                            			
                    				}
                    			});
                    			
                    		}

                            if(gig.getString(TAG_TIME)!="null")
                            	timeTF.setText("Time: "+gig.getString(TAG_TIME));
                            else
                            	timeTF.setText("");
                            
                            if(gig.getString(TAG_SUPPORT_ONE)!="null")
                            	supportTF.setText("Support: "+gig.getString(TAG_SUPPORT_ONE));
                            else
                            	supportTF.setText("");
                            
                            if(gig.getString(TAG_SUPPORT_TWO)!= "null")
                            	supportTF.append("\n "+gig.getString(TAG_SUPPORT_TWO));
                            
                            genreTF.setText("Genre: "+gig.getString(TAG_GENRE));
                            priceTF.setText("Price: €"+gig.getString(TAG_PRICE));
                            dateTF.setText("Date: "+gig.getString(TAG_DATE));
                            
                        }
                        else{
                        	
                        }
                    } 
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
 
            return null;
        }
        
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once got all details
            pDialog.dismiss();
            if(success == 0){
        		Toast.makeText(BandInfo.this, "Could not read database.", Toast.LENGTH_LONG).show();
            }
    		Toast.makeText(BandInfo.this, "" +artist, Toast.LENGTH_LONG).show();

        }

    }
}

