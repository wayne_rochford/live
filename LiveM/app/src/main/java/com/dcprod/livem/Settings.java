
 /*
 
 * 
 * SettingsMain.java
 * 
 * 
 * 
 * @reference https://developers.facebook.com/android/
 * @author Wayne Rochford, x11472332, Patrick O'Donnnell
 * 
 * REF @:http://thenewboston.org/list.php?cat=6
 */
package com.dcprod.livem;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;

public class Settings extends Activity implements OnClickListener {
	ImageButton im;
	CheckBox Rock;
	CheckBox Pop;
	CheckBox Metal;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	
		setContentView(R.layout.settings);

		Rock = (CheckBox)findViewById(R.id.checkBox1);
		 Rock.setOnCheckedChangeListener(listener);
		 
		Pop = (CheckBox)findViewById(R.id.checkBox2);
		 Pop.setOnCheckedChangeListener(listener);
		 
		Metal = (CheckBox)findViewById(R.id.checkBox3);
		 Metal.setOnCheckedChangeListener(listener);
		 
		 im = (ImageButton) findViewById(R.id.arr);
			im.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					startActivity(new Intent("com.dcprod.livem.MAIN"));//stars activity
					//Log.d("rock", Integer.toString(rock));
					finish();
				}
			});
		 
	}
		 

		private OnCheckedChangeListener listener = new OnCheckedChangeListener() {
			
			
	
			 public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				
				
			 if(isChecked){
			 switch(arg0.getId())
			   {
			    case R.id.checkBox1:
			          Rock.setChecked(true);
			          Pop.setChecked(false);
			          Metal.setChecked(false);
			          break;
			     case R.id.checkBox2:
			          Pop.setChecked(true);
			          Metal.setChecked(false);
			          Rock.setChecked(false);
			          break;
			    case R.id.checkBox3:
			          Metal.setChecked(true);
			          Pop.setChecked(false);
			          Rock.setChecked(false);
			          break;
			   }
			 }
		
		
	}
			/*	public void savePref(){
			 		pref = getSharedPreferences("SearchPreference", Context.MODE_PRIVATE);	
			 	    Editor editor = pref.edit();
			 	    editor.putInt("Choice1", rock);
			 	    editor.putInt("Choice2", metal);
			 	    editor.putInt("Choice3", pop);
			 	    editor.commit();
			 	}*/
		 
};

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
		}

		 
}


