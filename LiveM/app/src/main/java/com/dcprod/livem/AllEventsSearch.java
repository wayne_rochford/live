package com.dcprod.livem;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
 
public class AllEventsSearch extends ListActivity {
 
    // Progress Dialog
    private ProgressDialog pDialog;
 
    // Creating JSON Parser object
    JSONParser jParser = new JSONParser();
 
    ArrayList<HashMap<String, String>> productsList;
 
    // url to get all products list
    private static String url_all = "http://livem.site11.com/get_all_records.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "gigdata";
    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "artist";
    private static final String TAG_VENUE = "venue";
    private static final String TAG_TIME = "time";
    private static final String TAG_DATE = "date";
    private static final String TAG_GENRE ="genre";
    private static final String TAG_PROMOTER = "promoter";
    private static final String TAG_PRICE = "price";
    private static final String TAG_SUPPORT_ONE = "support_one";
    private static final String TAG_SUPPORT_TWO = "support_two";
    int success;
    
  
    // products JSONArray
    JSONArray products = null;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_events);
        
        ArrayList<ItemDetails> itemDetailsrrayList;
    	
    	Integer[] imgid = {
    			R.drawable.aikenbutton,
    			R.drawable.dmebutton,
    			R.drawable.smcd,
    			
    			};
 
        // Hashmap for ListView
        productsList = new ArrayList<HashMap<String, String>>();
 
        // Loading products in Background Thread
        new LoadAllProducts().execute();
 
        // Get listview
        ListView lv = getListView();
 
        // on seleting single product
        // launching Edit Product Screen
        
        lv.setOnItemClickListener(new OnItemClickListener() {
 
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // getting values from selected ListItem
                String pid = ((TextView) view.findViewById(R.id.id)).getText().toString();
 
                // Starting new intent
                Intent in = new Intent(getApplicationContext(),BandInfo.class);
                // sending pid to next activity
                in.putExtra(TAG_ID, pid);
 
                // starting new activity and expecting some response back
                startActivityForResult(in, 100);
            }
        });
 
    }
 
    // Response from Edit Product Activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // if result code 100
        if (resultCode == 100) {
            // if result code 100 is received
            // means user edited/deleted product
            // reload this screen again
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }
 
    }
 
    /**
     * Background Async Task to Load all product by making HTTP Request
     * */
    class LoadAllProducts extends AsyncTask<String, String, String> {
 
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AllEventsSearch.this);
            pDialog.setMessage("Loading gigs. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
 
        /**
         * getting All products from url
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all, "GET", params);
 
            // Check your log cat for JSON response
            Log.d("All Products: ", json.toString());
 
            try {
                // Checking for SUCCESS TAG
                success = json.getInt(TAG_SUCCESS);
 
                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    products = json.getJSONArray(TAG_PRODUCTS);
                    
                    // looping through All Products
                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);
 
                        // Storing each json item in variable
                        String id = c.getString(TAG_ID);
                        String name = c.getString(TAG_NAME);
                        String venue = c.getString(TAG_VENUE);
                        String time = c.getString(TAG_TIME);
                        String promoter = c.getString(TAG_PROMOTER);

                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();
                        
                        // adding each child node to HashMap key => value
                        map.put(TAG_ID, id);
                        map.put(TAG_NAME, name);
                        map.put(TAG_VENUE, venue);
                        map.put(TAG_TIME, time);
 
                        // adding HashList to ArrayList
                        productsList.add(map);
                    }
                } 
                else {
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
 
            return null;
        }
 
        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            pDialog.dismiss();

            if(success == 1){
                // updating UI from Background Thread
                runOnUiThread(new Runnable() {
                    public void run() {
                        /**
                         * Updating parsed JSON data into ListView
                         * */
                        ListAdapter adapter = new SimpleAdapter(
                        		AllEventsSearch.this, productsList,
                                R.layout.list_item, new String[] {TAG_ID, TAG_NAME, TAG_VENUE}, 
                                									new int[] {R.id.id, R.id.name, R.id.itemDescription });

                        // updating listview
                        setListAdapter(adapter);
                    }
                });}
                else{
            		Toast.makeText(AllEventsSearch.this, "The database could not be read.", Toast.LENGTH_LONG).show();
                }
 
        }
 
    }
}