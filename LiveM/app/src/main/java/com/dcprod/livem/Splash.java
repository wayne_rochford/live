
 /*
 
 * 
 * Splash.java
 * 
 * Date e.g. 4-11-13
 * 
 * @reference https://developers.facebook.com/android/
 * @author Wayne Rochford, x11472332
 * 
 * REF @:http://thenewboston.org/list.php?cat=6
 */
package com.dcprod.livem;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Splash extends Activity {

	ImageButton sb;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		
		sb = (ImageButton) findViewById(R.id.logobutton);
		
		//Starts main activity
		
		sb.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent p = new Intent(Splash.this, Main.class);
	               
                startActivityForResult(p, 0);
                finish();
				
			}
		});


		
		
	}


	
}

