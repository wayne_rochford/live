 /*
 
 * 
 * Facebook.java
 * 
 * 
 * @reference https://developers.facebook.com/android/
 * @author Wayne Rochford, x11472332
 * Ref https://developers.facebook.com/docs/android/login-with-facebook/
 */
package com.dcprod.livem;

 import android.os.Bundle;
 import android.support.v4.app.FragmentActivity;

public class Facebook extends FragmentActivity{
	

	private static final String TAG = "MainFragment";//constant for tag fragment
    private MainFragment mainFragment;


	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		// TODO Auto-generated method stub


		super.onCreate(savedInstanceState);
		setContentView(R.layout.facebook);


		
		if(savedInstanceState==null)
		{
			

			
		mainFragment = new MainFragment();
		getSupportFragmentManager()
		.beginTransaction()
		.add(android.R.id.content, mainFragment)
		.commit();
		
      
	
		}

		else
		{


			
		// Or set the fragment from restored state info
		mainFragment = (MainFragment) getSupportFragmentManager()
		.findFragmentById(android.R.id.content);
		
		
		
		

		}
		
	}




	
	}
		
	
	



	

	



