package com.dcprod.livem;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.Calendar;



//datapicker based off tutorial found at:
//http://www.mkyong.com/android/android-date-picker-example/
public class Search extends Activity {
    
	RadioGroup rGroup;
		RadioButton artBtn, dateBtn, priceBtn;
	
	EditText artistTxt, priceTxt;
	Button searchBtn;
	DatePicker datePick;
	
	private static final String TAG_SEARCH = "search";
	private static final String TAG_PRICE = "price";

	String search;
	String price;
	private int year,month,day;
	public  int monthSelected, daySelected;
	
	static final int DATE_DIALOG_ID = 0;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_screen);

		final RadioGroup rGroup = (RadioGroup)findViewById(R.id.searchRG);
		DatePicker datePick = (DatePicker) findViewById(R.id.datePicker);
		Button searchBtn = (Button) findViewById(R.id.searchBtn);
		RadioButton dateBtn = (RadioButton) findViewById(R.id.dateRbtn);
		
		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);
		
		dateBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//this is depreciated and should be done in a fragment
				showDialog(DATE_DIALOG_ID);
			}
		});
		
		searchBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				EditText artistTxt = (EditText) findViewById(R.id.artistTF);
				EditText priceTxt = (EditText) findViewById(R.id.priceTF);
				
				int radioButtonID = rGroup.getCheckedRadioButtonId();
				View radioButton = rGroup.findViewById(radioButtonID);
				int radioID = rGroup.indexOfChild(radioButton);
				
				//artist
				if (radioID == 0){
					if(artistTxt.getText().toString().equals("")){
						Intent i = new Intent(getApplicationContext(), AllEventsSearch.class);
						startActivity(i);
					}
					else{
						
						search = artistTxt.getText().toString();
		                Intent in = new Intent(getApplicationContext(),SearchAllActivity.class);
	                	// sending to next activity
	                	in.putExtra(TAG_SEARCH, search);
	                	startActivity(in);
	                	
	                	
					}
				}
				
				//date
				else if(radioID == 1){
	                Intent in = new Intent(getApplicationContext(),SearchAllActivity.class);
                	// sending to next activity
                	in.putExtra(TAG_SEARCH, search);
                	startActivity(in);
				}
				
				//price
				else if(radioID == 2){
					if(priceTxt.getText().toString().equals("")){
						Intent i = new Intent(getApplicationContext(), AllEventsSearch.class);
						startActivity(i);
					}
					else{
						
						price = priceTxt.getText().toString();
		                Intent in = new Intent(getApplicationContext(),SearchPrice.class);
	                	// sending to next activity
	                	in.putExtra(TAG_PRICE, price);
	                	startActivity(in);
	                	
					}
				}
				else{

					Toast.makeText(Search.this, "Please select a search option.", Toast.LENGTH_SHORT).show();

				}
			}
		});
		
	}
	
	protected Dialog onCreateDialog(int id){
		switch (id) {
		case DATE_DIALOG_ID:
		   return new DatePickerDialog(this, datePickerListener, year , month, day);
		}
		return null;
	}



	 private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
                 public void onDateSet(DatePicker view, int yearSelected, int monthOfYear, int dayOfMonth) {
                    yearSelected = yearSelected;
                    monthSelected = monthOfYear;
                    daySelected = dayOfMonth;
                    search = ""+yearSelected+"-"+(monthSelected+1)+"-"+daySelected;
                   Toast.makeText(getApplicationContext(), "Date selected is:"+daySelected+"-"+monthSelected+"-"+yearSelected, Toast.LENGTH_LONG).show();
                 }
    };

}
