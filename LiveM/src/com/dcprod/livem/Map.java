package com.dcprod.livem;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Map extends Activity {
	
	ImageButton im;
	GoogleMap mMap;
	
	
	final LatLng SPIRE = new LatLng(53.3498223, -6.260351);
	final LatLng O2 = new LatLng(53.3475, -6.2286111);
	final LatLng ACAD = new LatLng(53.3477952, -6.2634768);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
	
		
		mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
		        .getMap();
		
		//Move the camera instantly to hamburg with a zoom of 15.
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(SPIRE, 15));

		// Zoom in, animating the camera.
		mMap.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null); 
		 

		  Marker o2 = mMap.addMarker(new MarkerOptions()
		                            .position(O2)
		                            .title("O2 Arena")
		                            .snippet("Metallica")
		                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.fnote)));
		
		  Marker acad = mMap.addMarker(new MarkerOptions()
          .position(ACAD)
          .title("The Academy")
          .snippet("Children of Bodom")
          .icon(BitmapDescriptorFactory.fromResource(R.drawable.note)));
		  
		  mMap.setOnMarkerClickListener(new OnMarkerClickListener() {
	            @Override
				public boolean onMarkerClick(Marker o2) {
					// TODO Auto-generated method stub
	            	
	            	//int filter = 0;
	            	
	            	Intent y = new Intent(Map.this, ListViewImagesActivity.class);
	          //  	y.putExtra("Messenger", filter);//Packages the choice value to pass it over to the intent which is directed at the Maps class
	                startActivityForResult(y, 0);
	                finish();
					return false;
				}
	        });
		  
		  mMap.setOnMarkerClickListener(new OnMarkerClickListener() {
	            @Override
				public boolean onMarkerClick(Marker acad) {
					// TODO Auto-generated method stub
	            	
	            	//int filter = 1;
	            	
	            	Intent y = new Intent(Map.this, ListViewImagesActivity.class);
	            //	y.putExtra("Messenger", filter);//Packages the choice value to pass it over to the intent which is directed at the Maps class
					startActivityForResult(y, 0);
	                finish();
					return false;
				}
	        });
		
		  im = (ImageButton) findViewById(R.id.imageButton1);
		
im.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Intent y = new Intent(Map.this, Main.class);
	               
	                startActivityForResult(y, 0);
	                finish();
				
			}
		});
		
	}
	}
	
	


