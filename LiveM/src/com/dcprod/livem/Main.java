package com.dcprod.livem;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;

public class Main extends Activity {
	
	
	ImageButton im1,im2,im3,im4;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		
		im1 = (ImageButton) findViewById(R.id.imageButton4);
		im2 = (ImageButton) findViewById(R.id.imageButton2);
		im3 = (ImageButton) findViewById(R.id.imageButton1);
		im4 = (ImageButton) findViewById(R.id.imageButton3);
		
		im1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Intent y = new Intent(Main.this, About.class);
	             
	                startActivityForResult(y, 0);
	                finish();
	                
				
				
			}
		});
		
		im2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent y = new Intent(Main.this,Map.class);
	             
                startActivityForResult(y, 0);
                finish();
				
			}
		});
		
im3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent y = new Intent(Main.this,Search.class);
	             
                startActivityForResult(y, 0);
                finish();
				
			}
		});
		
		
		
	
	
	im4.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			Intent y = new Intent(Main.this,Settings.class);
             
            startActivityForResult(y, 0);
            finish();
			
		}
	});
	
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
