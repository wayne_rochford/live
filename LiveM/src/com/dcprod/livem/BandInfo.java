package com.dcprod.livem;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class BandInfo extends Activity {
	
	ImageButton mcd,tm,live,fb,ar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bandinfo);
		
		mcd = (ImageButton) findViewById(R.id.imageButton1);
		tm = (ImageButton) findViewById(R.id.imageButton2);
		live = (ImageButton) findViewById(R.id.imageButton3);
		fb = (ImageButton) findViewById(R.id.imageButton4);
		ar = (ImageButton) findViewById(R.id.imageButton5);
	
		
		mcd.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Toast.makeText(BandInfo.this, "Visit MCD.ie for more information", Toast.LENGTH_SHORT).show();
				
			}
		});
		
tm.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Toast.makeText(BandInfo.this, "Buy tickets on TicketMaster", Toast.LENGTH_SHORT).show();
				
				
			}
		});

live.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		Toast.makeText(BandInfo.this, "View the location on the Live Map", Toast.LENGTH_SHORT).show();
		
	}
});

fb.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		Toast.makeText(BandInfo.this, "Share this gig on Facebook", Toast.LENGTH_SHORT).show();
		
	}
});

ar.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		Intent y = new Intent(BandInfo.this, Main.class);
        
        startActivityForResult(y, 0);
        finish();
		
		
	}
});
		
		
		
		
		
	}
	
	

}
