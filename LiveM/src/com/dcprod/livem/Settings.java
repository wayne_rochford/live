package com.dcprod.livem;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;

public class Settings extends Activity {
	
	ImageButton im;
	CheckBox Rock ;
	CheckBox Pop ;
	CheckBox Metal ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
		
		Rock = (CheckBox)findViewById(R.id.checkBox1);
		 Rock.setOnCheckedChangeListener(listener);
		 
		Pop = (CheckBox)findViewById(R.id.checkBox2);
		 Pop.setOnCheckedChangeListener(listener);
		 
		Metal = (CheckBox)findViewById(R.id.checkBox3);
		 Metal.setOnCheckedChangeListener(listener);
		 
	}
		 
		 private OnCheckedChangeListener listener = new OnCheckedChangeListener() {
			 
			 public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
			 if(isChecked){
			 switch(arg0.getId())
			   {
			     case R.id.checkBox1:
			          Rock.setChecked(true);
			          Pop.setChecked(false);
			          Metal.setChecked(false);
			          break;
			     case R.id.checkBox2:
			          Pop.setChecked(true);
			          Metal.setChecked(false);
			          Rock.setChecked(false);
			          break;
			    case R.id.checkBox3:
			         Metal.setChecked(true);
			         Pop.setChecked(false);
			         Rock.setChecked(false);
			         break;
			   }
			 }
			  
			 
		
		im = (ImageButton) findViewById(R.id.arr);
		
		im.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent y = new Intent(Settings.this, Main.class);
	               
                startActivityForResult(y, 0);
                finish();
				
			}
		});
		
		
	}
	
		 
		 
};
		 
}


