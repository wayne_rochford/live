package com.dcprod.livem;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ListViewImagesActivity extends Activity {
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.whats);
        
    	
        
        ArrayList<ItemDetails> image_details = GetSearchResults();
        
        final ListView lv1 = (ListView) findViewById(R.id.listV_main);
        lv1.setAdapter(new ItemListBaseAdapter(this, image_details));
        
        lv1.setOnItemClickListener(new OnItemClickListener() {
        	@Override
        	
        	
        	public void onItemClick(AdapterView<?> a, View v, int position, long id) { 
        		Object o = lv1.getItemAtPosition(position);
        		
        		
        		
            	Intent y = new Intent(ListViewImagesActivity.this, BandInfo.class);
	               
                startActivityForResult(y, 0);
                finish();
            	
        	}  
        });
    }
    
    private ArrayList<ItemDetails> GetSearchResults(){
    	
    	
    	ArrayList<ItemDetails> results = new ArrayList<ItemDetails>();
    	
    
 	    
 	   ItemDetails item_details = new ItemDetails();

    		
        	item_details.setName("Metallica");
        	item_details.setItemDescription("O2 Arena - 02/03/14");
        	item_details.setPrice("Promoter: MCD");
        	item_details.setImageNumber(1);
        	results.add(item_details);
        	
        	item_details = new ItemDetails();
        	item_details.setName("KISS");
        	item_details.setItemDescription("O2 Arena - 01/04/14");
        	item_details.setPrice("Promoter: MCD");
        	item_details.setImageNumber(1);
        	results.add(item_details);
        	
        	item_details = new ItemDetails();
        	item_details.setName("Jay-Z");
        	item_details.setItemDescription("O2 Arena - 09/12/13");
        	item_details.setPrice("Promoter: MCD");
        	item_details.setImageNumber(1);
        	results.add(item_details);
        	
        	item_details = new ItemDetails();
        	item_details.setName("Lady GAGA");
        	item_details.setItemDescription("O2 Arena - 25/07/14");
        	item_details.setPrice("Promoter: MCD");
        	item_details.setImageNumber(1);
        	results.add(item_details);
    		
    	item_details = new ItemDetails();
    	item_details.setName("Children of Bodom");
    	item_details.setItemDescription("The Academy - 08/06/14");
    	item_details.setPrice("Promoter: DME");
    	item_details.setImageNumber(2);
    	results.add(item_details);
    	
 

		return results;
    	
    
    }
}